For this project it was decided to use a microservice structure.
- easy to replace or delete module
- easy to change additional services
- microservice is independent
- use bounded context
- scalability
- modules organised and microservice perfom pure function

For this project it was decided to use HTTP protocol and socket.io
HTTP protocol is used for building REST Ful API and implement CRUD operations.
HTTP is the most used protocol on the Internet and socket server is easy to implement with this protocol .
Socket.io is needed to control client orders, blog functionality etc.